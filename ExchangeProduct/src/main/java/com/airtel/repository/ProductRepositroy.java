package com.airtel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.airtel.model.Product;

@Repository
public interface ProductRepositroy extends JpaRepository<Product, String>{

}
