package com.airtel.repository;



import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.airtel.model.ExchangeOrder;

@Repository
public interface OrderRepository extends JpaRepository<ExchangeOrder, Integer>{

	List<ExchangeOrder> findByexchangeDate(Date orderDate);

}
