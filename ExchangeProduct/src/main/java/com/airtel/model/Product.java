package com.airtel.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Product {
	@Id
	private String id;
	private String productName;
	private Double retailPrice;
	private Double discountedPrice;
	private String description;
	private String productRating;
	private String overallRating;
	private String brand;
}
