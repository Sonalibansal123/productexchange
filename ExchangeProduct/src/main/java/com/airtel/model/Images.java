package com.airtel.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
@Data
public class Images {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String url;
	@ManyToOne
	@JoinColumn(name = "productId")
	private Product product;
	
}
