package com.airtel.DTO;

import lombok.Data;

@Data
public class ProductDTO {

	private String id;
	private String productName;
	private Double retailPrice;
	private Double discountedPrice;
	private String description;
	private String productRating;
	private String overallRating;
	private String brand;
}
