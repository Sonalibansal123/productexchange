package com.airtel.DTO;

import java.util.Date;


import lombok.Data;

@Data
public class OrderVO {
	private Integer id;
	private Boolean valid;
	private Double exchangePrice;
	private Date exchangeDate;
	private ProductDTO product;
	private UserDTO user;
}
