package com.airtel.DTO;

import java.util.Date;
import java.util.List;

import com.airtel.model.Product;

import lombok.Data;

@Data
public class OrderDTO {

	List<OrderVO> orderList;
}
