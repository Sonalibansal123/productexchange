package com.airtel.service;

import com.airtel.DTO.UserDTO;
import com.airtel.model.User;

public interface UserService {

	User getUser(Integer id);

	UserDTO getUser(User user);

}
