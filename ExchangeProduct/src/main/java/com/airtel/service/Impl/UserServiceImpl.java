package com.airtel.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.airtel.DTO.UserDTO;
import com.airtel.model.User;
import com.airtel.repository.UserRepository;
import com.airtel.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userRepo;
	@Override
	public User getUser(Integer id) {
		// TODO Auto-generated method stub
		return userRepo.getById(id);
	}
	@Override
	public UserDTO getUser(User user) {
		// TODO Auto-generated method stub
		UserDTO userDto = new UserDTO();
		userDto.setAddress(user.getAddress());
		userDto.setEmail(user.getEmail());
		userDto.setFullName(user.getFullName());
		userDto.setId(user.getId());
		return userDto;
	}

}
