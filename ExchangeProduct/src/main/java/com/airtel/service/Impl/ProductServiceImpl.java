package com.airtel.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.airtel.DTO.ProductDTO;
import com.airtel.model.Product;
import com.airtel.repository.ProductRepositroy;
import com.airtel.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	ProductRepositroy productRepo;
	
	@Override
	public List<ProductDTO> getProductList() {
		
		List<Product> productList = productRepo.findAll();
		List<ProductDTO> productDtoList = new ArrayList<ProductDTO>();
		for(Product product : productList) {
			productDtoList.add(getProduct(product));
		}
		return productDtoList;
	}

	@Override
	public Product getProduct(String id) {
		// TODO Auto-generated method stub
		return productRepo.getById(id);
	}

	@Override
	public ProductDTO getProduct(Product product) {
		ProductDTO productDTO = new ProductDTO();
		productDTO.setBrand(product.getBrand());
		productDTO.setDescription(product.getDescription());
		productDTO.setDiscountedPrice(product.getDiscountedPrice());
		productDTO.setId(product.getId());
		productDTO.setOverallRating(product.getOverallRating());
		productDTO.setProductName(product.getProductName());
		productDTO.setProductRating(product.getProductRating());
		productDTO.setRetailPrice(product.getRetailPrice());
		return productDTO;
	}

	

}
