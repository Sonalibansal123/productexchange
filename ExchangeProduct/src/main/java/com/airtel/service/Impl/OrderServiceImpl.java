package com.airtel.service.Impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.airtel.DTO.OrderDTO;
import com.airtel.DTO.OrderVO;
import com.airtel.model.ExchangeOrder;
import com.airtel.model.Product;
import com.airtel.model.User;
import com.airtel.repository.OrderRepository;
import com.airtel.service.OrderService;
import com.airtel.service.ProductService;
import com.airtel.service.UserService;

@Service
public class OrderServiceImpl implements OrderService{

	@Autowired
	ProductService productService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	OrderRepository orderRepo;
	
	@Override
	public String saveExchangeOrder(OrderVO order) {
		// TODO Auto-generated method stub
		String toreturn="";
		try {
		Product product = productService.getProduct(order.getProduct().getId());
		User user = userService.getUser(order.getUser().getId());
		ExchangeOrder exchange = new ExchangeOrder();
		if(order.getExchangePrice()<=product.getRetailPrice())
			{
			exchange.setValid(false);
			toreturn+="InValid request payment denied";
			
			}
		else
			{
			exchange.setValid(true);
			toreturn+="Valid request payment accepted";
			}
		
		exchange.setExchangeDate(new Date());
		exchange.setExchangePrice(order.getExchangePrice());
		exchange.setProduct(product);
		exchange.setUser(user);
		orderRepo.save(exchange);
		}
		catch(Exception e) {
			return "Failure: Issue occurred while saving the request";
		}
		return toreturn;
	}

	@Override
	public OrderDTO getAllOrdersByDate(Date orderDate) {
		// TODO Auto-generated method stub
		
		List<ExchangeOrder> orderList = orderRepo.findByexchangeDate(orderDate);
		List<OrderVO> orderVOlist = new ArrayList<OrderVO>();
		for (ExchangeOrder order : orderList) {
			OrderVO orderVo = new OrderVO();
			orderVo.setExchangeDate(order.getExchangeDate());
			orderVo.setExchangePrice(order.getExchangePrice());
			orderVo.setId(order.getId());
			orderVo.setValid(order.getValid());
			orderVo.setProduct(productService.getProduct(order.getProduct()));
			orderVo.setUser(userService.getUser(order.getUser()));
			orderVOlist.add(orderVo);
		}
		OrderDTO orderDto = new OrderDTO();
		orderDto.setOrderList(orderVOlist);
		return orderDto;
		 
				
	}

}
