package com.airtel.service;

import java.util.List;

import com.airtel.DTO.ProductDTO;
import com.airtel.model.Product;

public interface ProductService {

	List<ProductDTO> getProductList();

	Product getProduct(String id);

	ProductDTO getProduct(Product product);


}
