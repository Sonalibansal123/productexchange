package com.airtel.service;

import java.util.Date;

import com.airtel.DTO.OrderDTO;
import com.airtel.DTO.OrderVO;

public interface OrderService {

	String saveExchangeOrder(OrderVO order);

	OrderDTO getAllOrdersByDate(Date orderDate);

}
