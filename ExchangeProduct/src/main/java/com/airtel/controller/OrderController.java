package com.airtel.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.airtel.DTO.OrderDTO;
import com.airtel.DTO.OrderVO;
import com.airtel.service.OrderService;

@RestController
@RequestMapping("/order")
public class OrderController {
	
	@Autowired
	OrderService orderService;
	
	@PostMapping("/saveExchangeOrder")
	public String saveExchangeOrder(@RequestBody OrderVO order) {
		return orderService.saveExchangeOrder(order);
	}
	
	@GetMapping("/getAllOrdersByDate/{orderDate}")
	public OrderDTO getAllOrdersByDate(@PathVariable @DateTimeFormat(iso=ISO.DATE) Date orderDate) {
		return orderService.getAllOrdersByDate(orderDate);
	}

}
